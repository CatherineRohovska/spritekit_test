//
//  GameScene.m
//  Test_SpriteKit
//
//  Created by Admin on 12/28/15.
//  Copyright (c) 2015 Admin. All rights reserved.
//

#import "GameScene.h"
static const uint32_t playerCategory     =  0x1 << 0;
static const uint32_t monsterCategory    =  0x1 << 1;

@interface GameScene ()
{
    int i;
}

@end
@implementation GameScene

// calculation
static inline CGPoint rwAdd(CGPoint a, CGPoint b) {
    return CGPointMake(a.x + b.x, a.y + b.y);
}

static inline CGPoint rwSub(CGPoint a, CGPoint b) {
    return CGPointMake(a.x - b.x, a.y - b.y);
}

static inline CGPoint rwMult(CGPoint a, float b) {
    return CGPointMake(a.x * b, a.y * b);
}

static inline float rwLength(CGPoint a) {
    return sqrtf(a.x * a.x + a.y * a.y);
}

static inline CGPoint rwNormalize(CGPoint a) {
    float length = rwLength(a);
    return CGPointMake(a.x / length, a.y / length);
}
//
-(void)didMoveToView:(SKView *)view {
    /* Setup your scene here */
//    SKLabelNode *myLabel = [SKLabelNode labelNodeWithFontNamed:@"Chalkduster"];
//    
//    myLabel.text = @"Hello, World!";
//    myLabel.fontSize = 45;
//    myLabel.position = CGPointMake(CGRectGetMidX(self.frame),
//                                   CGRectGetMidY(self.frame));
    
 //   [self addChild:myLabel];
}
-(id)initWithSize:(CGSize)size {
    if (self = [super initWithSize:size]) {
        i=0;
        // setup scene with screen size
        NSLog(@"Size: %@", NSStringFromCGSize(size));
        
        // setup clear color
        self.backgroundColor = [SKColor colorWithRed:0.0 green:1.0 blue:1.0 alpha:1.0];
        
        // setup player sprite
        self.player = [SKSpriteNode spriteNodeWithImageNamed:@"player"];
        [self addChild:self.player];
        self.player.position =  CGPointMake(20, 200);
      
        //setup physics
        self.physicsWorld.gravity = CGVectorMake(0,0);
        self.physicsWorld.contactDelegate = self;
        // setup player collider
        self.player.physicsBody = [SKPhysicsBody bodyWithRectangleOfSize:self.player.size]; // 1
        self.player.physicsBody.dynamic = YES; // 2
        self.player.physicsBody.categoryBitMask = playerCategory; // 3
        self.player.physicsBody.contactTestBitMask = monsterCategory ; // 4
        self.player.physicsBody.collisionBitMask = 0; // 5
        
    }
    return self;
}
-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    /* Called when a touch begins */
    
    for (UITouch *touch in touches) {
        CGPoint location = [touch locationInNode:self];
        CGPoint currentPos = self.player.position;
        NSTimeInterval duration = sqrt((location.x-currentPos.x)*(location.x-currentPos.x)+(location.y-currentPos.y)*(location.y-currentPos.y))/200;
        SKAction *action = [SKAction moveTo:location duration:duration];
        //[self.player removeAllActions];
        [self.player  runAction:action completion:^{
            [self.player removeActionForKey:@"walkingInPlace"];
        }];
        ///setup moving textures. It can be done earlier without reloading whole array
        SKTextureAtlas *moveAnimatedAtlas = [SKTextureAtlas atlasNamed:@"texture"];
        NSString *textureName = [NSString stringWithFormat:@"player"];
        SKTexture *temp = [moveAnimatedAtlas textureNamed:textureName];
        NSMutableArray *walkFrames = [NSMutableArray array];
        [walkFrames addObject:temp];
        textureName = [NSString stringWithFormat:@"monster"];
        temp = [moveAnimatedAtlas textureNamed:textureName];
        
        [walkFrames addObject:temp];
        NSArray* _walkingframes = walkFrames; // create animation chain of texture frames
        //run animation
        
        [self.player runAction:[SKAction repeatActionForever:
                          [SKAction animateWithTextures:_walkingframes
                                           timePerFrame:0.1f
                                                 resize:NO
                                                restore:YES]] withKey:@"walkingInPlace"];
      
 
    }
}
-(void) changeColor
{
    i++;
    self.player.colorBlendFactor = 0.5f;
    NSLog(@"i: %i",i);
    switch (i) {
        case 1:
             self.player.color = [UIColor blueColor];
             [self.player runAction:[SKAction scaleTo:0.5 duration:0.25]];
            break;
        case 2:
            self.player.color = [UIColor redColor];
             [self.player runAction:[SKAction scaleTo:1.2 duration:0.25]];
            break;
        case 3:
            self.player.color = [UIColor yellowColor];
             [self.player runAction:[SKAction scaleTo:0.6 duration:0.25]];
            break;
        case 4:
            self.player.color = [UIColor whiteColor];
            [self.player runAction:[SKAction scaleTo:1 duration:0.25]];
            i=0;
            break;
        default:
            break;
    }
}

-(void)update:(CFTimeInterval)currentTime {
    /* Called before each frame is rendered */
    int random = arc4random() % 100;
    if (random==50) {
 
        NSLog(@"enemy created");
        [self addMonster];
    }
    if (random==10)
    {//rotate whole scene
        NSLog(@"%i",random);
        self.view.transform = CGAffineTransformRotate( self.view.transform, M_PI);
        
    }
   
    // NSLog(@"%i",random);
}

- (void)addMonster {
    
    // Create sprite
    SKSpriteNode * monster = [SKSpriteNode spriteNodeWithImageNamed:@"monster"];
    
    // Determine where to spawn the monster along the Y axis
    int minY = monster.size.height / 2;
    int maxY = self.frame.size.height - monster.size.height / 2;
    int rangeY = maxY - minY;
    int actualY = (arc4random() % rangeY) + minY;
    
    // Create the monster slightly off-screen along the right edge,
    // and along a random position along the Y axis as calculated above
    [self addChild:monster];
    monster.position = CGPointMake(self.frame.size.width + monster.size.width/2, actualY);
    
    
    // Determine speed of the monster
    int minDuration = 2.0;
    int maxDuration = 4.0;
    int rangeDuration = maxDuration - minDuration;
    int actualDuration = (arc4random() % rangeDuration) + minDuration;
    
    // Create the actions
    SKAction * actionMove = [SKAction moveTo:CGPointMake(-monster.size.width/2, actualY) duration:actualDuration];
    SKAction * actionMoveDone = [SKAction removeFromParent];
    [monster runAction:[SKAction sequence:@[actionMove, actionMoveDone]]];
    //setup monster collider
    monster.physicsBody = [SKPhysicsBody bodyWithRectangleOfSize:monster.size]; // 1
    monster.physicsBody.dynamic = YES; // 2
    monster.physicsBody.categoryBitMask = monsterCategory; // 3
    monster.physicsBody.contactTestBitMask = playerCategory; // 4
    monster.physicsBody.collisionBitMask = 0; // 5
    
}
//collide with monster action
- (void)player:(SKSpriteNode *)player didCollideWithMonster:(SKSpriteNode *)monster {
    NSLog(@"Hit");
   // [player removeFromParent];
    [monster removeFromParent];
}
//check collision
- (void)didBeginContact:(SKPhysicsContact *)contact
{
    // 1
    SKPhysicsBody *firstBody, *secondBody;
    
    if (contact.bodyA.categoryBitMask < contact.bodyB.categoryBitMask)
    {
        firstBody = contact.bodyA;
        secondBody = contact.bodyB;
    }
    else
    {
        firstBody = contact.bodyB;
        secondBody = contact.bodyA;
    }
    
    //  if the two bodies that collide are the player and monster
    if ((firstBody.categoryBitMask & playerCategory) != 0 &&
        (secondBody.categoryBitMask & monsterCategory) != 0)
    {
        [self player:(SKSpriteNode *) firstBody.node didCollideWithMonster:(SKSpriteNode *) secondBody.node];
    }
}
@end
