//
//  GameViewController.h
//  Test_SpriteKit
//

//  Copyright (c) 2015 Admin. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <SpriteKit/SpriteKit.h>

@interface GameViewController : UIViewController
- (IBAction)colorChangeButtonClick:(id)sender;
@property (weak, nonatomic) IBOutlet SKView *secondView;

@end
