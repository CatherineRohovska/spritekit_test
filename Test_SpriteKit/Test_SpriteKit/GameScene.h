//
//  GameScene.h
//  Test_SpriteKit
//

//  Copyright (c) 2015 Admin. All rights reserved.
//

#import <SpriteKit/SpriteKit.h>

@interface GameScene : SKScene
<SKPhysicsContactDelegate>
@property (nonatomic) SKSpriteNode * player;
-(void) changeColor;
@end
